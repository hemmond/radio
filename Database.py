import sqlite3

class Database:
    """Manages operations with sqlite database."""
    
    def __init__(self, dbName):
        self.dbTables = ["sounds"]
        self.dbName = dbName
        connection = sqlite3.connect(dbName)
        
        # test if database has proper tables - and make sure that it has at the end. 
        self.initTables()
        
        #connection = self.connection
        #dbCursor = connection.cursor()
        #for row in dbCursor.execute("SELECT name FROM sqlite_master WHERE type='table';" ):
            #print(row);
        
        connection.close()
    
    def initTables(self):
        """Initializes database if necessary. After this function returns, database is ready."""
        connection = sqlite3.connect(self.dbName)
        dbCursor = connection.cursor()
        
        ### get existing tables
        tables = []
        for row in dbCursor.execute("SELECT name FROM sqlite_master WHERE type='table';" ):
            tables.append(row[0])
        
        ### find missing tables and insert them. 
        for table in self.dbTables:
            if( not (table in tables) ):
                if(table == "sounds"):
                    ### name     = Name of the sound track
                    ### start    = time when password starts to be accepted
                    ### end      = last second when password is accepted
                    ### password = name of file to be played - file must be in ./audio folder and in wav format. 
                    
                    # Create table with required columns
                    dbCursor.execute('''CREATE TABLE sounds (id integer PRIMARY KEY, name text, start integer, end interger, password text, filename text, played integer)''')
                    
                    # Insert values to the database - duplicate the next row with new data if necessary. 
                    dbCursor.execute('''INSERT INTO `sounds` (id,name,start,end,password,filename,played) VALUES (1,'01 Prvni',3600,4500,'HESLO','1_prvni.wav',0);''')
                    
                    print("creating table sounds")
                connection.commit()
        connection.close()
        
    def getAllRecords(self):
        """Returns all records from table sounds"""
        connection = sqlite3.connect(self.dbName)
        dbCursor = connection.cursor()
        
        retArray = []
        for row in dbCursor.execute("select * from sounds" ):
            retDict = {}
            retDict[0]      = int(row[0])
            retDict["id"]   = int(row[0])
            
            retDict[1]      = row[1]
            retDict["name"] = row[1]
            
            retDict[2]          = int(row[2])
            retDict["start"]    = int(row[2])
            
            retDict[3]      = int(row[3])
            retDict["end"]  = int(row[3])
            
            retDict[4]          = row[4]
            retDict["password"] = row[4]
            
            retDict[5]          = row[5]
            retDict["filename"] = row[5]
            
            playedVal = False
            if( int(row[6]) == 1 ):
                playedVal = True
            retDict[6]          = playedVal
            retDict["played"]   = playedVal
            
            retArray.append(retDict)
        connection.close()
        return retArray

    def getPasswords(self, time=None):
        """Returns all passwords in the database. 
        
        if time is specified, it returns only passwords of records which can be playedin the specified time. 
        """
        connection = sqlite3.connect(self.dbName)
        dbCursor = connection.cursor()
                
        if(time != None):
            dbResult = dbCursor.execute("select id,password from sounds where start <= ? and end >= ? and played == 0", ( str(time), str(time) ) )
        else:
            dbResult = dbCursor.execute("select id,password from sounds")
            
        retDict = {}
        for row in dbResult:
            ### returns tuple (id,password)
            retDict[ int(row[0]) ] = row[1]
        
        #print("DB.GetPasswords: ")
        #print(retDict)
        
        connection.close()
        return retDict
            
    def getFileName(self, rowId):
        """Returns name of the file for specified database ID"""
        connection = sqlite3.connect(self.dbName)
        dbCursor = connection.cursor()
        
        print("getFileName: get name by ID "+str(rowId) )
        
        retVal = ""
        for row in dbCursor.execute("select filename from sounds where id == ? ", ( str(rowId), ) ):
            print(row)
            retVal = row[0]
        
        connection.close()
        print("getFileName: returning filename "+str(retVal) )
        return retVal
    
    def setPlayedRecord(self, rowId, value=True):
        """Sets "played" flag to record with rowId database ID. 
        
        Optional parameter value marks, to which boolean value the "played" flag shall be set.
        """
        connection = sqlite3.connect(self.dbName)
        dbCursor = connection.cursor()
        
        value = bool(value)
        
        if( value == True ):
            value = 1
        else:
            value = 0
        
        dbCursor.execute("update sounds set played = ? where id == ? ", ( str(value), str(rowId) ) )
        connection.commit()
        connection.close()
        return
        
    def setStartTime(self, rowId, time):
        """Changes start time of record with database ID equals to rowId to time."""
        self._setTime(rowId, time, True)
    
    def setEndTime(self, rowId, time):
        """Changes end time of record with database ID equals to rowId to time."""
        self._setTime(rowId, time, False)
    
    def _setTime(self, rowId, time, isStartTime):
        """Auxiliary function which sets start/end time. 
        
        Do not use this method directly, use setStartTime or setEndTime.
        """
        if( bool(isStartTime) == True ):
            column = "start"
        else:
            column = "end"
        
        connection = sqlite3.connect(self.dbName)
        dbCursor = connection.cursor()
        
        time = int(time)
        
        if(time>=0):
            if(isStartTime == True):
                dbCursor.execute("update sounds set start = ? where id == ? ", ( str(time), str(rowId) ) )
            else:
                dbCursor.execute("update sounds set end = ? where id == ? ", ( str(time), str(rowId) ) )
            connection.commit()
        connection.close()
        return
        