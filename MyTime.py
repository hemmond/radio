class MyTime:
    """Class taking care of game time measuring."""
    
    def __init__(self, running=False):
        """Default constructor. 
        
        running argument (optional) is whether time measuring should be started or not (default is false).
        """
        self.running = bool(running)
        self.time = 0       # time in seconds
        
    def start(self):
        """Starts the game time clock"""
        print("MyTime: start.")
        self.running = True
        
    def stop(self):
        """Stops the game time clock"""
        print("MyTime: stop.")
        self.running = False
        
    def tick(self):
        """Tick of the game clock. 
        
        Is called every second, but only increments time if game time clock are running.
        """
        if(self.running == True):
            self.time += 1
    
    def setTime(self, time):
        """Manualy sets game time to specified time."""
        print("MyTime: timeshift na "+str(time) )
        self.time = time
    
    def getTime(self):
        """Returns current game time"""
        return int(self.time)
    
    ### timeFormat is biggest sent value (s, m, h, d)
    def getJson(self, timeFormat='d', convertedValue=None):
        """Returns JSON containing time in specified format. Also appends if game time clock are running or not. 
        
        timeFormat - this argument represents highest time measuring unit which shall be inserted into JSON
        convertedValue - if this argument is set, function will return JSON with this time. If not set, function will return JSON with current game time. 
        """
        if(convertedValue == None):
            convertedValue = self.time
        
        payload = {}
        payload["running"] = self.running
        
        if( timeFormat == 's'):
            payload["ss"] = convertedValue
        elif( timeFormat == 'm'):
            minutes, seconds = divmod( convertedValue, 60)
            payload["ss"] = seconds
            payload["mm"] = minutes
        elif( timeFormat == 'h'):
            minutes, seconds = divmod( convertedValue, 60)
            payload["ss"] = seconds
            hours, minutes = divmod( minutes, 60)
            payload["mm"] = minutes
            payload["hh"] = hours
        elif( timeFormat == 'd' ):
            minutes, seconds = divmod( convertedValue, 60)
            payload["ss"] = seconds
            hours, minutes = divmod( minutes, 60)
            payload["mm"] = minutes
            days, hours = divmod( hours, 24)
            payload["hh"] = hours
            payload["dd"] = days
        return payload
