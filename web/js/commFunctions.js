/** Send message of custom type with JSON payload 
 @param msgType type of message (string)
 @param jsonPayload message payload (json in string) */
function sendConnectMessage(msgType, jsonPayload) {
    var socket = io.connect('http://' + document.domain + ':' + location.port);
    socket.on('connect', function() {
        socket.emit(msgType, jsonPayload);
    });
}

function sendMessageOnce(msgType, payload) {
    var socket = io.connect('http://' + document.domain + ':' + location.port);
    socket.emit(msgType, payload);
}
