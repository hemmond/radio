import os
import time

class Player:
    """Class represents sound player."""
    
    def __init__(self, dbObject):
        """Constructor. 
        
        dbObject -- reference to global object of Database class.
        """
        self.main_dir = os.path.split(os.path.abspath(__file__))[0]
        self.db = dbObject
        self.playing = False
        self.ledLight = None

    def play(self, recordId):
        """Plays audio file specified in database on row recordId.
        
        This method plays noise sound before and after every audio file and also sets "played" flag for that record in the database. """
        print("Play: "+str(recordId) )
        filename = self.db.getFileName(recordId)
        filePath = os.path.join(self.main_dir, "audio/"+filename)
        noisePath = os.path.join(self.main_dir, "audio/sum_10s.wav")
        
        self.playing = True
        print("PLAYing: "+filePath)
        print("aplay " + str(noisePath))
        self.lightOn()
        os.system("aplay " + str(noisePath) )
        print("radio start")
        os.system("aplay " + str(filePath) )
        print("radio end")
        os.system("aplay " + str(noisePath) )
        self.lightOff()
        print("playing finished")
        
        self.db.setPlayedRecord(recordId)
        
        self.playing = False
    
    def lightOn(self):
        """Lights up led light"""
        if( self.ledlight != None ):
            self.ledlight.lightOn()

    def lightOff(self):
        """Turns off led light"""
        if( self.ledlight != None ):
            self.ledlight.lightOff()

    def setLedLight(self, light):
        """Stores reference to LedLight object to control led light"""
        self.ledlight = light

    def isPlaying(self):
        """Returns if any audio file is now being played."""
        return self.playing
