import RPi.GPIO as GPIO
import signal, threading
import time

LOW     = 0
HIGH    = 1

class MagneticKeyboard(threading.Thread):
    """Handles magnetic keyboard I/O"""

    def __init__(self, callbackFunction=None):
        """Default constructor. 
        
        callbackFunction is function which will be called when change on keyboard occurs, with string containing current "pressed" buttons.
        If this callback is not set during constructor, it can be later set through registerCallback method. 
        """
        threading.Thread.__init__(self)
        
        self.keyboardThread_stop = threading.Event()
        
        self.keyboard_rows = ( 3, 5, 7, 11 )
        self.keyboard_cols = ( 13, 15, 19, 21, 23, 29, 31 )
        
        if(callbackFunction != None):
            self.callback = callbackFunction
        
        self.keypad = (
            ("A", "B", "C", "D", "E", "F", "G"),
            ("H", "I", "J", "K", "L", "M", "*"),
            ("N", "O", "P", "Q", "R", "S", "T"),
            ("U", "V", "W", "X", "Y", "Z", "#")
            )   #acces by self.keypad[row][col]
        
        self.lastState = []
        
        GPIO.setmode(GPIO.BOARD)
        GPIO.cleanup()
        self.ledLight = LedLight(37)
                
        for i in self.keyboard_rows:
            GPIO.setup( i, GPIO.OUT )
            GPIO.output( i, LOW)
        
        for i in self.keyboard_cols:
            GPIO.setup( i, GPIO.IN, pull_up_down=GPIO.PUD_DOWN )
        
    def registerCallback(self, callbackFunction):
        """registers function which will be called when change on keyboard occurs, with string containing current "pressed" buttons."""
        self.callback = callbackFunction
    
    def getLedLight(self):
        """Returns object of LedLight class."""
        return self.ledLight
    
    def stop(self):
        """ This method stops the thread monitoring magnetic keyboard. """
        if self.isAlive():
            self.keyboardThread_stop.set()
        time.sleep(2)
        GPIO.cleanup()

    def run(self):
        """This method monitors magnetic keyboard and if change occures, it passes new combination to callback class. 
        
        This method is overriden method of threading.Thread """
        while not self.keyboardThread_stop.is_set():
            currentState = []
#            print("loop")
            for row in range( 0, len(self.keyboard_rows) ):
                GPIO.output( self.keyboard_rows[row], HIGH )
#                print("row "+str(row) )
                time.sleep(0.01)

                for col in range( 0, len(self.keyboard_cols) ):
                    val = GPIO.input(self.keyboard_cols[col])
#                    print("col "+str(col)+"with value "+str(val) )
                    if(val == HIGH):
                        currentState.append( self.keypad[row][col] )

                time.sleep(0.01)                
                GPIO.output( self.keyboard_rows[row], LOW )
            
            if(self.lastState != currentState):
                self.ledLight.blink()
                self.callback( "".join(currentState) )
            else:
                time.sleep(0.1)
            
            self.lastState = currentState

class LedLight:
    """Class taking care of Led light"""
    
    def __init__(self, gpioPin):
        """Constructor. 
        
        gpioPin is RPi pin number on which led light is connected. 
        """
        self.led_light = gpioPin
        GPIO.setup( self.led_light, GPIO.OUT)
        GPIO.output( self.led_light, LOW)
    
    def blink(self):
        """Blinks with led lignt (for 0,5seconds)."""
        GPIO.output( self.led_light, HIGH )
        time.sleep(0.5)
        GPIO.output( self.led_light, LOW)
    
    def lightOn(self):
        """Lights up led light"""
        GPIO.output( self.led_light, HIGH )
        
    def lightOff(self):
        """Turns off led light"""
        GPIO.output( self.led_light, LOW)
