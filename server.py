#!/usr/bin/env python3 

import os, sys
from flask import Flask, render_template, send_from_directory, request, url_for, redirect
from flask_socketio import SocketIO, emit, join_room
import logging

from apscheduler.schedulers.background import BackgroundScheduler as BackgroundScheduler

from Database import Database
from MyTime import MyTime
from Player import Player
from MagneticKeyboard import MagneticKeyboard, LedLight

app      = Flask('eventServer', template_folder='web', static_url_path='')
app.config['SECRET_KEY'] = 'secret!_mnn62zds7if9rk487ye3kvc73lkefi3foz8qe59'
socketio = SocketIO(app)

VERSION         = '1.0'
DB_NAME         = "test.db"

main_dir = os.path.split(os.path.abspath(__file__))[0]
DB_PATH = os.path.join(main_dir, DB_NAME)

scheduler   = BackgroundScheduler()
database    = Database(DB_PATH)
myTime      = MyTime()
player      = Player(database)
keyboard    = None
ledlight    = None

lastPwd     = ""

### Handle any socketio connection
@socketio.on('connection')
def handle_connection(json):
    print('received connection json: ' + str(json))

    #Připojení klienti, jejich sledování a přístup k jejich úpravě. 
    join_room('clock')

### Functions to set the clock
@socketio.on('adminAction')
def handle_adminAction(json):
    print('received action: ' + str(json))
    if( json["action"] == "start" ):
        myTime.start()
    elif(json["action"] == "stop"):
        myTime.stop()
    elif(json["action"] == "set"):
        for key, value in json.items():
            if(value == ""):
                json[key] = 0
        print(json)
        myTime.setTime( int(json["dd"])*24*60*60+int(json["hh"])*60*60+int(json["mm"])*60+int(json["ss"]) )

@app.route('/passwd/<password>')
def checkPasswordWeb(password):
    checkPassword(password)
    #return render_index()
    return redirect('/')

def checkPassword(password):
    def sortPass(pwd):
        password = str(pwd).lower()
        return sorted(password)    ### Array of characters
    
    global lastPwd
    lastPwd = str(password)
    
    print( "CheckPassword: Checking password "+str(password) )
    
    if( player.isPlaying() ):
        print("CheckPassword: Password ignored due to playing. ")
        return
    
    inputPass = sortPass(password)
    
    probablePasswords = database.getPasswords( myTime.getTime() )
    
    recordId = None
    
    for key, value in probablePasswords.items():
        testPass = sortPass(value)
        
        if( testPass == inputPass ):
            recordId = int(key)
            print("CheckPassword: Password match: "+str(recordId) )
            break
    
    if(recordId != None):
        print("CheckPassword: Calling player with "+str(recordId) )
        player.play( int(recordId) )
    
    return

@app.after_request
def add_header(r):
    r.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    r.headers["Pragma"] = "no-cache"
    r.headers["Expires"] = "0"
    r.headers['Cache-Control'] = 'public, max-age=0'
    return r

### This function serves CSS files from /web/css/ folder.
@app.route('/css/<path:path>')
def send_css(path):
    return send_from_directory('web/css', path)

### This function serves JS files from /web/js/ folder.
@app.route('/js/<path:path>')
def send_js(path):
    return send_from_directory('web/js', path)

@app.route('/', methods=['POST'])
def parseForm():
    def jsonToSeconds(json):
        return int(json["dd"])*24*60*60 + int(json["hh"])*60*60 + int(json["mm"])*60 + int(json["ss"])
    
    recordId = int(request.form['id'])

    start = {}
    start["dd"] = request.form['start_dd']
    start["hh"] = request.form['start_hh']
    start["mm"] = request.form['start_mm']
    start["ss"] = request.form['start_ss']
    
    end = {}
    end["dd"] = request.form['end_dd']
    end["hh"] = request.form['end_hh']
    end["mm"] = request.form['end_mm']
    end["ss"] = request.form['end_ss']
    
    if( "played" in request.form):
        played = True
    else:
        played = False
    
    # set Start time if it was set. 
    changed  = False
    for key,value in start.items():
        if(value == ""):
            start[key] = 0
            #changed = False
        elif( int(value) >=0):
            changed  = True
    
    if(changed == True):
        time = jsonToSeconds( start )
        database.setStartTime( recordId, time )
    
    # set End time if it was set
    changed = False
    for key,value in end.items():
        if(value == ""):
            end[key] = 0
            #changed = False
        elif( int(value) >=0):
            changed  = True
    
    if(changed == True):
        time = jsonToSeconds( end )
        database.setEndTime( recordId, time )
    
    # set played value:
    database.setPlayedRecord( recordId, played)
    
    return render_index()
    

### Returns admin interface to browser
@app.route('/')
def render_index():
    html_data = "\n";
    html_data += htmlTableHeader()
    for row in database.getAllRecords():
        html_data += "<tr id=\"row_"+str(row["id"])+"\">"
        html_data += "<form action=\"./\" method=\"post\">"
        
        
        html_data += "\t<td class=\"name\">"
        html_data += htmlHiddenInput("id", str(row["id"]) )
        html_data += htmlTextField("name", row["name"])
        html_data += "</td>\n"
        
        html_data += "\t<td class=\"time start\">"
        html_data += htmlTextTime("start", row["start"])
        #html_data += htmlInputTime( "start_" + str( row["id"] ) )
        html_data += htmlInputTime( "start" )
        html_data += "</td>\n"
        
        html_data += "\t<td class=\"time end\">"
        html_data += htmlTextTime("end", row["end"])
        #html_data += htmlInputTime( "end_" + str( row["id"] ) )
        html_data += htmlInputTime( "end" )
        html_data += "</td>\n"
        
        html_data += "\t<td class=\"password\">"
        html_data += htmlTextField("password", row["password"])
        html_data += "</td>\n"
        
        html_data += "\t<td class=\"filename\">"
        html_data += htmlTextField("filename", row["filename"])
        html_data += "</td>\n"
        
        html_data += "\t<td class=\"played\">"
        html_data += htmlCheckbox( "played", bool( row["played"] ) )
        html_data += "</td>\n"
        
        html_data += "\t<td class=\"send\">"
        html_data += "<input type=\"submit\" value=\"SET\">"
        html_data += "</td>\n"
        
        html_data += "</form>"
        html_data += "</tr>\n"
    return render_template('index.html', html_data = html_data, last_pwd = lastPwd )

### returns hidden input element
def htmlHiddenInput(fieldId, value):
    html_data = ""
    html_data += "<input type=\"hidden\" id=\""+str(fieldId)+"\" "
    html_data += "name=\""+str(fieldId)+"\" value=\""+str(value)+"\" "
    html_data += "/>"
    return html_data

### returns table header
def htmlTableHeader():
    return "<tr><th>Popis</th><th>Cas zacatku</th><th>Cas konce</th><th>Heslo</th><th>Nazev souboru</th><th>Prehrano</th><th></th></tr>\n"

### returns HTML checkbox
def htmlCheckbox(fieldId, value):
    html_data = ""
    html_data += "<input type=\"checkbox\" id=\""+str(fieldId)+"\" "
    html_data += "name=\""+str(fieldId)+"\" value=\"played\" "
    if( bool(value) == True ):
        html_data += "checked"
    html_data += "/>"
    return html_data

### returns HTML code for span text field
def htmlTextField(fieldId, containedText):
    return "<span id=\""+str(fieldId)+"\">"+str(containedText)+"</span><br/>"

### returns HTML code for text representation of time in format d:hh:mm:ss
def htmlTextTime(fieldId, shownTime):
    html_data = ""
    convertedTime = myTime.getJson('d', shownTime)
    html_data += "<span id=\""+str(fieldId)+"\">"
    html_data += str(convertedTime["dd"])+":"
    html_data += str(convertedTime["hh"]).rjust(2, '0')+":"
    html_data += str(convertedTime["mm"]).rjust(2, '0')+":"
    html_data += str(convertedTime["ss"]).rjust(2, '0')
    html_data += "</span><br/>"
    return html_data

### returns HTML code for number input field
def htmlInputNumber(fieldId, placeholder="", minValue=None, maxValue=None, width=None):
    html_data = ""
    html_data += "<input type=\"number\" id=\""+str(fieldId)+"\" name=\""+str(fieldId)+"\" placeholder=\""+str(placeholder)+"\""
    if( minValue != None ):
        html_data += " min=\""+str(int(minValue))+"\""
    if( maxValue != None ):
        html_data += " max=\""+str(int(maxValue))+"\""
    if( width != None ):
        html_data += " style=\"width: "+str(int(width))+"px;\""
    html_data += "/>"
    return html_data

### returns HTML code for d:hh:mm:ss input fields
def htmlInputTime(fieldId):
    fieldWidth=60
    html_data = ""
    html_data += htmlInputNumber(fieldId+"_dd", "dd", minValue=0, width=fieldWidth)
    html_data += " : "
    html_data += htmlInputNumber(fieldId+"_hh", "hh", minValue=0, maxValue=23, width=fieldWidth)
    html_data += " : "
    html_data += htmlInputNumber(fieldId+"_mm", "mm", minValue=0, maxValue=59, width=fieldWidth)
    html_data += " : "
    html_data += htmlInputNumber(fieldId+"_ss", "ss", minValue=0, maxValue=59, width=fieldWidth)
    return html_data

@scheduler.scheduled_job('interval', seconds=1)
def broadcastTime():
    myTime.tick()
    json = myTime.getJson()
    json["password"] = str(lastPwd)
    socketio.emit('gameData', json, room='clock')

if __name__ == "__main__":
    log = logging.getLogger('werkzeug')
    log.setLevel(logging.ERROR)
    
    print("Radio, version: "+VERSION)
    print("created for HP Larp Brno")
    print("created by hemmond, 2019")
    
#    global keyboard
#    global ledlight
    keyboard = MagneticKeyboard( checkPassword )
    ledlight = keyboard.getLedLight()
    player.setLedLight(ledlight)

    #scheduler.start()
    
    try:
        #socketio.run(app, debug=True, use_reloader=False)
        scheduler.start()
        keyboard.start()
        socketio.run(app, debug=True, use_reloader=False, host='0.0.0.0')
    except (KeyboardInterrupt, SystemExit):
        scheduler.shutdown()
        keyboard.stop()
